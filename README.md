# FFTWhelper

### Usage
Add the package to your project via

```add https://gitlab.com/hattl/FFTWhelper.jl```

in you pkg environment.

### Tutorial
Read through the usecase, tests and the documentation! 

### Building

To build the documentation run

```julia --project=../. make.jl```

from the ```docs``` directory. This will build the HTML documentation and output it to the ```doc/build/``` folder.