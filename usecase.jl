using FFTWhelper
using FFTW
using Printf

Nx= 16
dim = 2

# create complex and real fftw helper
cfftwhelper = getfftwhelper(Nx, dim; type="complex")
rfftwhelper = getfftwhelper(Nx, dim; type="real")
contfftwhelper = getfftwhelper( length(cfftwhelper) )

printfftwhelper(cfftwhelper)
printfftwhelper(rfftwhelper)
printfftwhelper(contfftwhelper)

# rebin complex helper to 4 bins
# Note: if some bins turn out to be empty one gets an error!
bins = 4
rebinnedfftwhelper = rebin(cfftwhelper, bins)
println("REBINNED FFTW HELPER:")
printfftwhelper(rebinnedfftwhelper)

# create advanced fftw helper
(zerolevel, cornerlevels, bulklevels) = getadvfftwhelper(Nx, dim)

if dim==1 A = rand(Nx) end 
if dim==2 A = rand(Nx,Nx) end 
if dim==3 A = rand(Nx,Nx,Nx) end 

Ak = rfft(A)

# print zero mode
println("print zero mode")
@show Ak[1]
# print corner modes
println("print corner modes")
for i in 1:length(cornerlevels)
    for d in 1:cornerlevels[i].udeg
        println( [Ak[cornerlevels[i].ind[d][c]] for c in 1:cornerlevels[i].copies[d] ] )
    end
end
# print bulk modes
println("print bulk modes")
for i in 1:length(bulklevels)
    for d in 1:bulklevels[i].udeg
        println( [Ak[bulklevels[i].ind[d][c]] for c in 1:bulklevels[i].copies[d] ] )
    end
end