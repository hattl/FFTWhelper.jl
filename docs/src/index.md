# FFTWhelper.jl Documentation

```@contents
```

## Functions
```@docs
getfftwhelper(Nx,sim;type)
rebin(fftwhelper::Vector{fftwlevel},bins::Int64) 
printfftwhelper(fftwhelper::Vector{fftwlevel})
```

## Index

```@index
```