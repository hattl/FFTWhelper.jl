# run this file from bash to create/update docs: julia --project=../. make.jl
using Documenter, FFTWhelper

makedocs(sitename="FFTWhelper.jl")