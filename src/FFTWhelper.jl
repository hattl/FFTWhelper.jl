#############################################################################################################################
# Creates fftwhelper and creates a rebin function
#############################################################################################################################
module FFTWhelper

export getfftwhelper
export getadvfftwhelper
export rebin
export printfftwhelper

#using Random
using StatsBase
using FFTW

# super fast method to obtain the indices of the gridpoints that have the same k value
function addcounts_dict!(cm::Dict{Float64,Array{Int}}, x)
    for (inx,v) in enumerate(x)
        index = Base.ht_keyindex2!(cm, v)
        if index > 0
            @inbounds push!(cm.vals[index],inx)
        else
            cm[v] = [inx]
        end
    end
    return cm
end

function getksquaredcomplex(Nx,dim)
	# fftw ordering
	nvalues = zeros(Int64,Nx)
	for i in 0:(Nx-1)
	    if (i>Nx/2)
	        nvalues[i+1] = -(Nx-i) # julia indexing
	    else
	        nvalues[i+1] = i  # julia indexing
	    end
	end

	# ksquared
	vol = Nx^dim
	ksquared = zeros(Float64, vol)
	for i in 0:(vol-1)
	    for d in 0:(dim-1)
		#C code: coordinate = i/((int)std::pow(Nx,d)) % Nx;
		coordinate = Int64( mod( round(i/Nx^d, RoundDown) , Nx ) )
	        ksquared[i+1] += (2-2*cos(nvalues[coordinate+1]*(2*pi)/Nx))# julia indexing
	    end
	    ksquared[i+1] = round(ksquared[i+1], digits=6)
	end
	return ksquared
end

function getksquaredreal(Nx,dim)
		# for real ffts:
		#	- the n values of the first coordinate range from 0...N/2+1
		#	- the n values of the other coordinates range from 0...N/2+1,-N/2,...1
		# fftw ordering for other coordinates
		nvalues = zeros(Int64,Nx)
		for i in 0:(Nx-1)
			if (i<=Nx/2)
				nvalues[i+1] = -(Nx-i) # julia indexing
			else
				nvalues[i+1] = i  # julia indexing
			end
		end
		# fftw ordering for first coordinate
		nvalues2 = zeros(Int64,Nx)
		for i in 0:Int(Nx/2)
			nvalues2[i+1] = i  # julia indexing
		end
	
		# ksquared
		vol = Int(Nx^(dim-1) * (Nx/2 + 1))
		ksquared = zeros(Float64, vol)
		for i in 0:(vol-1)
			# first dimension
			#d=0
			coordinate = Int64( mod( i , (Nx/2+1) ) ) #first coordinate goes from 0 to Nx/2 -> Nx/2+1 elements
			ksquared[i+1] += (2-2*cos(nvalues2[coordinate+1]*(2*pi)/Nx))# julia indexing
			# the rest dimension
			for d in 1:(dim-1)
				#C code: coordinate = i/((int)std::pow(Nx,d)) % Nx;
				coordinate = Int64( mod( round(i/((Nx/2+1)*Nx^(d-1)), RoundDown) , Nx ) ) #other coordinates go from 0 to Nx-1 -> Nx elements
				ksquared[i+1] += (2-2*cos(nvalues[coordinate+1]*(2*pi)/Nx))# julia indexing
			end
			ksquared[i+1] = round(ksquared[i+1], digits=6)
		end
	return ksquared
end

#
# struct to hold level, degeneracy, and array of indices matching level
#
export fftwlevel
struct fftwlevel
        lev::Float64
        lev2::Float64
        deg::Int64
        ind::Vector{Int64}
	# for binned helper
        lev_do::Float64
        lev_up::Float64
end

export advfftwlevel
struct advfftwlevel
    lev::Float64
    lev2::Float64
    udeg::Int64
    copies::Vector{Int64}
    ind::Vector{Vector{Int64}}
end

"""
    getfftwhelper(Nx, dim; type)

	Arguments: Nx::Int - Number of sites per dimension, dim::Int - dimension, type::String - complex or real 
	Returns an array of type fftwlevel specific to Nx, dim and the type of fft it is used for (complex or real).

	Real DFTs (a DFT of a real array) have "hermitian" fourier transforms.
	Using this property allows to perform DFTs approx twice as fast and with only half the memory of complex DFTs. 

	From the discretized Laplace operator in FT space to first order (!) one gets
		k_L^2(n_i)= sum_i (2 - 2 cos(n_i (2pi)/Nx)) 
	Where the sum ranges over the dimensions and n_i denotes the fft n values in direction i.
	The fftwhelper allows to loop through all values of k_L^2 and to get the indices of the lattice points that have this k_L^2 value.

	FFTW odering for complex DFT:
		Complex N^d array <-> Complex N^d array
		for each dimension the n values range from 0,1,...(N/2-1),-N/2,-N/2+1,...1	

	FFTW odering for real DFT:
		Real N^d array <-> complex (N/2+1)*N^(d-1) array
		for the first dimension the n values range from 0,1,...(N/2-1),N/2
		for all the other dimensions the n values range from 0,1,...(N/2-1),-N/2,-N/2+1,...1	
	
	Each level has the following properties:
        - lev::Float64  -> sqrt of lev2 
        - lev2::Float64 -> see above 
        - deg::Int64 -> Numbers of indices with the same value of lev (or lev2)
        - ind::Vector{Int64} -> Vector with the indices that have the same value of lev (or lev2)
	# for binned helper
        - lev_do::Float64 -> in case of a binned helper: lower edge of the bin
        - lev_up::Float64 -> in case of a binned helper: upper edge of the bin
	
	CAUTION WITH REAL DFTs:
	In principle real DFTs should have half of the degeneracies of complex DFTs.
	But this is not the case. It has more! The reason is the underlying FFTW algorithm. 
	Therfore, some numbers are stored twice in the k space lattice.
	That is perfectly fine if one always loops over all of them and treats them equally.
	BUT it could lead to small errors if one wants to average over all k_L values of a lattice
	(As it is done in classical statistical simulations!),
	since the numbers that appear more often than they should will essentially get a sligthly higher weight when averaged.
	This errors reduce with the lattice size, though.
"""
function getfftwhelper(Nx, dim ; type="complex")
    println("Calculating " * type * " fftwhelper..."); flush(stdout)

	if Nx==1
		fftwhelper = Vector{fftwlevel}(undef, 1)
		fftwhelper[1] = fftwlevel( 0, 0, 1, [1], 0, 0)
	else
		@assert iseven(Nx) "Nx needs to be event for fftwhelper to work!"

		if type=="complex" ksquared=getksquaredcomplex(Nx,dim) end
		if type=="real" ksquared=getksquaredreal(Nx,dim) end

		# get unique levels and deg
		cm = Dict{Float64,Array{Int}}() # pairs of (ksquared, array of indices with same ksquared val)
		dict = addcounts_dict!(cm,ksquared) # populate keys and values
		sorteddict = sort(collect(dict), by = x->x[1]) # sort by ksquared values
		levels2 = first.(sorteddict)
		indexarray = last.(sorteddict)

		# set up the fftwhelper 
		fftwhelper = Vector{fftwlevel}(undef, length(levels2))
		for j in 1:length(levels2)
			fftwhelper[j] = fftwlevel( round(sqrt(levels2[j]),digits=6), levels2[j], length(indexarray[j]), indexarray[j], sqrt(levels2[j]),  sqrt(levels2[j]))
		end

		if type=="complex" vol = Nx^dim end
		if type=="real" vol = Int((Nx/2+1)*Nx^(dim-1)) end
		if sum([ element.deg for element in fftwhelper])!==vol println("BASIC CHECK OF FFTW HELPER FAILED!!!"); flush(stdout) end
	end

	return fftwhelper
end

function getidx_corn(Nx,sdim)
    # returns dict (index, equivalient indices)
    if sdim==1 A = rand(Nx) end
    if sdim==2 A = rand(Nx,Nx) end
    if sdim==3 A = rand(Nx,Nx,Nx) end

    # calc rfft of random matrix and find 
    #B = abs.(imag.(rfft(A)))
    B = abs.( round.(imag.(rfft(A)), digits=10 ))

    cm = Dict{Float64,Array{Int}}() # pairs of (ksquared, array of indices with same ksquared val)
    dict = addcounts_dict!(cm,B) # populate keys and values

    if length(dict[0]) != 2^sdim println("Error: Nyqist is wrong") end

    return dict[0][2:end]
end

function getdictofduplicateentries(Nx,sdim)
    # returns dict (index, equivalient indices)
	#Random.seed!(12345)
    if sdim==1 A = rand(Nx) end
    if sdim==2 A = rand(Nx,Nx) end
    if sdim==3 A = rand(Nx,Nx,Nx) end

    # calc rfft of random matrix and find 
    #B = abs.(real.(rfft(A))) # + im*abs.(imag.(rfft(A)))
    B = abs.( round.(real.(rfft(A)), digits=10 )) # + im*abs.(imag.(rfft(A)))
    #uvals = unique(B)

    cm = Dict{Float64,Array{Int}}() # pairs of (ksquared, array of indices with same ksquared val)
    #dict = addcounts_dict!(cm,real.(B)) # populate keys and values
    dict = addcounts_dict!(cm,B) # populate keys and values

    return Dict(  (a,b) for (a,b) in zip([ sort(a)[1] for a in values(dict)], values(dict) )   )
end

"""
    getadvfftwhelper(Nx, sdim)

	returns zeromode, cornermodes, bulkmodes.

	- the first two are collection of indices with real numbers (one dof), the last one has complex numbers (two dof)
	- fftw doubles some bulkmodes, therfore we have the copies property
"""
function getadvfftwhelper(Nx, sdim)
    println("Calculating advanced fftwhelper..."); flush(stdout)
	fftwhelper = getfftwhelper(Nx,sdim; type="real")
	#1) zero level
	zerolevel = advfftwlevel( 0, 0, 1, [1], [[1]])
	#2) nyquist level (without zero mode)
	idx_nyqu = getidx_corn(Nx,sdim)
	if sdim == 1 cornerlevels = Vector{advfftwlevel}(undef, 1) end
	if sdim == 2 cornerlevels = Vector{advfftwlevel}(undef, 2) end 
	if sdim == 3 cornerlevels = Vector{advfftwlevel}(undef, 3) end
	cnt=1
	for i in 1:length(fftwhelper)
    	if (fftwhelper[i].lev2 in [4,8,12]) # contains nyquist thing
    	    # filter
    	    nyquatlevel = filter(e->e ∈ idx_nyqu,fftwhelper[i].ind)
    	    cornerlevels[cnt] = advfftwlevel( fftwhelper[i].lev, fftwhelper[i].lev2, length(nyquatlevel), ones(Int64, length(nyquatlevel)), [ [el] for el in nyquatlevel] )
    	    cnt += 1
    	end
	end
	#3) bulk level
	thedict = getdictofduplicateentries(Nx,sdim)
	doubledindices = Any[]
	for a in values(thedict) 
	    if length(a) > 1
	        append!(doubledindices,a[2:end]...) # only the copies
	    end
	end
	if sdim == 1 bulklevels = Vector{advfftwlevel}(undef, length(fftwhelper)-2) end # less the 0 and maximum mode
	if sdim == 2 bulklevels = Vector{advfftwlevel}(undef, length(fftwhelper)-2) end 
	if sdim == 3 bulklevels = Vector{advfftwlevel}(undef, length(fftwhelper)-2) end
	cnt=1
	for i in 2:(length(fftwhelper)-1)
	    # get rid of nyquist indices
	    indi = filter(e->e∉idx_nyqu,fftwhelper[i].ind)
	    # get rid of double indices
	    uniqueindices = filter(e->e∉doubledindices,indi)
	    # get it
	    newindices = [thedict[h] for h in uniqueindices]
	    copies = [length(h) for h in newindices]
	    bulklevels[cnt] = advfftwlevel( fftwhelper[i].lev, fftwhelper[i].lev2, length(copies), copies, newindices )
	    cnt += 1
	end

	# Basic check
	dof_zero = 1 # real zero mode
	dof_corn = sum( [lev.udeg for lev in cornerlevels] ) # real nyquist modes
	dof_bulk = sum( [lev.udeg for lev in bulklevels] ) * 2 # complex bulk modes 
	if (dof_zero + dof_corn + dof_bulk) !== Nx^sdim 
		println("BASIC CHECK OF ADVFFTW HELPER FAILED!!!")
		println(" Vol: " * string(Nx^sdim ) * ", dof: " * string(dof_zero + dof_corn + dof_bulk) * "!!!")	
		println(" dof_corn: ", dof_corn )
		println(" dof_bulk: (contains factor of 2) ", dof_bulk )
	end

	return (zerolevel, cornerlevels, bulklevels)
end

"""
    getfftwhelper(nrmomenta)

	Arguments: nrmomenta::Int - Number of momenta
	Functions returns fftwhelper for "continuum" fourietransforms. Created for compatibiltiy with lattice implementations
"""
function getfftwhelper(nrmomenta)
    println("Calculating continuum fftwhelper..."); flush(stdout)

	fftwhelper = Vector{fftwlevel}(undef, nrmomenta)
	for i in 1:nrmomenta
		mom = i*pi/nrmomenta
		fftwhelper[i] = fftwlevel( mom, mom^2, 1, [i], mom, mom)
	end

	return fftwhelper
end
"""
	rebin(fftwhelper::Vector{fftwlevel},bins::Int64) 

	returns a rebinned fftw helper with bins number of bins. (the 0th bin is not binned, since it is a special one)

"""
function rebin(fftwhelper::Vector{fftwlevel},bins::Int64) # bins includes 0 bin - bins should be odd for later rebinning in postprocessing!
	if mod(bins,2) ==0
		println("bins needs to be odd....odding it"); flush(stdout)
		bins += 1
	end
	if bins>length(fftwhelper)
		println("tooo many bins for this fftwhelper..returning unbinned one"); flush(stdout)
		return fftwhelper
	end

	# calc bin edges of all but the zero bin
	lev_low = fftwhelper[1].lev
	lev_hig = fftwhelper[end].lev
	spacing = (lev_hig-lev_low)/(bins-1) # exclude zero bin
	edge_do = Vector{Float64}(undef, bins)
	edge_up = Vector{Float64}(undef, bins)
	for j in 2:bins
		edge_do[j] = lev_low + (j-2) * spacing #edge_do[2] = lev_low
		edge_up[j] = lev_low + (j-1) * spacing #edge_up[2] = lev_low + spacing, edge_up[bins] = lev_low + (bins-1)*spacing = lev_hig
	end

	# recast old levels to pure vector - later needed
	oldlevels = [element.lev for element in fftwhelper] 

	# create new fftwhelper - number of levels is bins
	fftwhelpernew = Vector{fftwlevel}(undef, bins)
	# add zero energy bin
	fftwhelpernew[1] = fftwhelper[1]
	for j in 2:bins
		fftwlevelindicesinbin = findall(x -> edge_do[j]<x<=edge_up[j], oldlevels) # get indices of oldlevels, whose values are inbetween do and up
		tmpdeg = sum([ fftwhelper[idx].deg for idx in fftwlevelindicesinbin])
		tmplevel = 0.5 * (fftwhelper[minimum(fftwlevelindicesinbin)].lev + fftwhelper[maximum(fftwlevelindicesinbin)].lev) 
		tmpind = Vector{Int64}(undef, tmpdeg)
		iter = 1
		for i in fftwlevelindicesinbin 
			tmpind[iter:iter+length(fftwhelper[i].ind)-1] .= fftwhelper[i].ind
			iter += length(fftwhelper[i].ind)
		end
		fftwhelpernew[j] = fftwlevel( tmplevel, tmplevel^2, tmpdeg, tmpind, fftwhelper[minimum(fftwlevelindicesinbin)].lev, fftwhelper[maximum(fftwlevelindicesinbin)].lev)
	end
	
	# basic check
	sumold = sum( [f.deg for f in fftwhelper])
	sumnew = sum( [f.deg for f in fftwhelpernew])
	#println(sumold, " -  " ,sumnew)
	if sumold!==sumnew println("BASIC CHECK OF REBIN OF FFTW HELPER FAILED!!!"); flush(stdout) end

	return fftwhelpernew
end
"""
    printfftwhelper(fftwhelper::Vector{fftwlevel})

	prints the levels, degeneracies and corresponding indices of the passed fftwhelper

"""
function printfftwhelper(fftwhelper::Vector{fftwlevel})
	for j in 1:length(fftwhelper)
		print("lev: ", fftwhelper[j].lev, "/ deg: ", fftwhelper[j].deg, "/ ind: ", fftwhelper[j].ind)
		println()
	end
end

end
