using FFTWhelper
using FFTW
using Printf

function compare_complex_real_helper(Nx,dim)
    # get helpers
    cfftwhelper = getfftwhelper(Nx, dim; type="complex")
    rfftwhelper = getfftwhelper(Nx, dim; type="real")

    # some checks  
    if length(cfftwhelper) != length(rfftwhelper)
        println("ERROR: complex and real helper have NOT same number of levels")
        return false
    end

    for i in 1:length(cfftwhelper) 
        if cfftwhelper[i].lev2 != rfftwhelper[i].lev2
            println("ERROR: complex and real helper have NOT same level values")
            return false
        end
    end

    # calc an fft
    mytype = Float64
    # initialize A in x space
    if dim==1 A = rand(mytype, Nx) end
    if dim==2 A = rand(mytype, Nx,Nx) end
    if dim==3 A = rand(mytype, Nx,Nx,Nx) end

    # Ac and Ar are A in k space
    Ac = fft(A)
    Ar = rfft(A)

    # modify Ac and Ar (each element times its lev2)
    for i in 1:length(cfftwhelper) 
        for j in 1:cfftwhelper[i].deg
            ind = cfftwhelper[i].ind[j]
            Ac[ind] *= cfftwhelper[i].lev2 
        end
    end
    for i in 1:length(rfftwhelper) 
        for j in 1:rfftwhelper[i].deg
            ind = rfftwhelper[i].ind[j]
            Ar[ind] *= rfftwhelper[i].lev2 
        end
    end

    # go back to x space
    Acc = ifft(Ac)
    Arr = irfft(Ar, Nx)

    # compare results
    maxdiff = maximum(abs.(real(Acc) - Arr)) 
    if !isapprox(0,maxdiff;atol=1e-4)
        println("ERROR: Non negliglible difference between complex and real helper applied to Matrix! Case: Nx: ", Nx, ", dim: ", dim)
        println(" * Max is difference   : ", maxdiff)
        return false
    else
        return true
    end
end