using FFTWhelper
using Test

include("compare_complex_real_helper.jl")

@testset "FFTWhelper.jl" begin
    @test compare_complex_real_helper(8,1) # Nx=8, sidm=1
    @test compare_complex_real_helper(16,2)
    @test compare_complex_real_helper(32,3)
end
